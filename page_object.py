from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys


class Page:
    def __init__(self, driver: webdriver.Chrome):
        self.driver = driver


class YandexMainPage(Page):

    search_css = '.search2__input .input__control'

    def search(self, text):
        search_input = self.driver.find_element_by_css_selector(self.search_css)
        WebDriverWait(self.driver, 5).until(lambda x: search_input.is_displayed(), 'Нет строки поиска')
        search_input.send_keys(text)
        mini_suggest = self.driver.find_element_by_css_selector('.mini-suggest__popup')
        WebDriverWait(self.driver, 5).until(lambda x: mini_suggest.is_displayed(), 'Нет таблицы с подсказками')
        search_input.send_keys(Keys.ENTER)

    def open_pictures(self):
        img_first_list = self.driver.find_element_by_css_selector('[data-id*="images"]')
        assert img_first_list, 'Ссылка "Картинки" отсутствует на странице'
        img_first_list.click()
        tabs = self.driver.window_handles
        self.driver.switch_to.window(tabs[1])
        assert 'https://yandex.ru/images/' in self.driver.current_url, 'Не перешли на страницу с картинками'


class YandexSearchPage(Page):

    def check_url_in_results(self, url):
        first_five = self.driver.find_elements_by_css_selector('.link_theme_outer')[:5]
        flag = False
        for my_href in first_five:
            if url in my_href.get_attribute('href'):
                flag = True
        assert flag, f'В первых 5 ссылках нет {url}'

    def check_table_results(self):
        table_results = self.driver.find_element_by_css_selector('.serp-list_left_yes')
        assert table_results.is_displayed(), 'Таблица с результатами поиска не появилась'


class YandexPicturePage(Page):

    def open_first_section(self):
        first_section = self.driver.find_element_by_css_selector('.PopularRequestList-Item_pos_0')
        first_section_text = first_section.find_element_by_css_selector('.PopularRequestList-SearchText').text
        first_section.click()
        WebDriverWait(self.driver, 5).until((lambda x: (first_section_text in self.driver.title)), 'Открылся неверный текст')

    def open_first_picture(self):
        first_pic = self.driver.find_element_by_css_selector('.serp-item__link')
        WebDriverWait(self.driver, 5).until(expected_conditions.visibility_of_element_located((By.CSS_SELECTOR, '.serp-item__link')))
        first_pic.click()
        first_pic_big = self.driver.find_element_by_css_selector('.MMImage-Preview')
        WebDriverWait(self.driver, 5).until(expected_conditions.visibility_of_element_located((By.CSS_SELECTOR, '.MMImage-Preview')))
        assert first_pic_big.is_displayed(), 'Картинка открылась'
        return first_pic_big.get_attribute('src')

    def click_button_next(self, first_pic_big_item):
        button_next = self.driver.find_element_by_css_selector('.MediaViewer_theme_fiji-ButtonNext')
        button_next.click()
        second_pic_big_item = self.driver.find_element_by_css_selector('.MMImage-Preview').get_attribute('src')
        assert second_pic_big_item != first_pic_big_item, 'Картинка не изменилась на следующую'

    def click_button_prev(self, first_pic_big_item):
        button_prev = self.driver.find_element_by_css_selector('.MediaViewer_theme_fiji-ButtonPrev')
        button_prev.click()
        now_pic_big_item = self.driver.find_element_by_css_selector('.MMImage-Preview').get_attribute('src')
        assert now_pic_big_item == first_pic_big_item, 'Картинка не поменялась на первоначальную'
