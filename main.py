from selenium import webdriver
import unittest
from page_object import YandexSearchPage, YandexMainPage, YandexPicturePage


class TestYandexMain(unittest.TestCase):

    driver = None

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome()
        cls.driver.maximize_window()

    def setUp(self):
        self.driver.get('https://www.yandex.ru/')

    def test_01_search(self):
        url_first = self.driver.current_url
        yandex_main_page = YandexMainPage(self.driver)
        yandex_main_page.search('Тензор')
        url_second = self.driver.current_url
        assert url_second != url_first, 'Таблица с результатами поиска не появилась'
        yandex_search_page = YandexSearchPage(self.driver)
        yandex_search_page.check_table_results()
        yandex_search_page.check_url_in_results('tensor.ru')

    def test_02_picture_link(self):
        work_picture = YandexMainPage(self.driver)
        work_picture.open_pictures()
        yandex_check_pictures = YandexPicturePage(self.driver)
        yandex_check_pictures.open_first_section()
        first_pic_big_item = yandex_check_pictures.open_first_picture()
        yandex_check_pictures.click_button_next(first_pic_big_item)
        yandex_check_pictures.click_button_prev(first_pic_big_item)

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()
        cls.driver.quit()


if __name__ == '__main__':
    unittest.main()
